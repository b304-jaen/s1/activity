package com.zuitt.wdc0043;
import java.text.DecimalFormat;
import java.util.Scanner;

public class User {


    public static void main(String[] args){

    String firstName;
    String lastName;
    double firstSubject;
    double secondSubject;
    double thirdSubject;

    Scanner user = new Scanner(System.in);
    Scanner grade = new Scanner(System.in);

    System.out.println("First Name:");
    firstName = user.nextLine();
    System.out.println("Last Name:");
    lastName = user.nextLine();

    System.out.println("First Subject Grade:");
    firstSubject = grade.nextDouble();
    System.out.println("Second Subject Grade:");
    secondSubject = grade.nextDouble();
    System.out.println("Third Subject Grade:");
    thirdSubject = grade.nextDouble();

    double avg = ((firstSubject + secondSubject + thirdSubject)/3);


    System.out.println("Good day, " + firstName + lastName);
    System.out.println("Your grade average is: " + new DecimalFormat("#").format(avg));



    }



}
